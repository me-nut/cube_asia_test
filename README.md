# Case Study E-Commerce Data Engineering Pipeline
Design a robust data engineering pipeline that sources data from e-commerce marketplace scrapers, processes the data, and stores it in a way that supports efficient querying and analysis. Data is extracted from multiple e-commerce marketplaces through a set of web scrapers. The scraped data is initially stored in an AWS S3 bucket in JSON format.

## Assumption
- We are dealing with small-medium size of data
- We are a start-up company

## 1. Pipeline Design
![alt text](reference/data_pipeline.png)

### 1.1. Component
The design includes following components:
- Source data on S3
- Transformation script on AWS Lambda
- Transformed data on S3
- Ingestion script on AWS Lambda
- Available data as tables on Redshift

#### 1.1.1. S3 (Scraped data) 
Raw scraped data is stored in an AWS S3 bucket in JSON format.
#### 1.1.2. Custom Script on AWS Lambda (Script A)
- A custom script deployed on AWS Lambda which downloads scraped data from S3, transforms the data and saves it to S3 as parquet files.   
- These functions can be scheduled to run daily or as per scraping frequency.
#### 1.1.3. S3 (Transformed data)
Transformed data is stored in an AWS S3 bucket in parquet format to improve query performance and storage efficiency. 
#### 1.1.4. Custom Script on AWS Lambda (Script B) 
A custom script for loading data from S3 to Redshift.

#### Script A
This is a custom script that needs to be able to handle data from various marketplaces so we should have,
- An extractor, downloads scraped data from S3
- A base transformer class which will be inheritted by its children
- Transformer classes based on marketplaces eg, ShopeeTransformer, LazadaTransformer
- Each transformer handles different schema, it's responsible for transforming scraped data to a target schema
- A Saver, saves data back to S3

#### Script B
- A custom script deployed on AWS Lambda.
- Triggered when a file is put on a transformed bucket (After an execution of Script A)
- This script is responsible of loading data from the file to Redshift tables.

### 1.2 Reason and Factor
I wish to outline my rationale for selecting this architecture, according to the following reasons:

#### 1.2.1 Simplicity
This architecture is simple and minimal. It can be easily established and operated. Moreover, I believe that most use cases could be resolved by this simple architecture.

#### 1.2.2 Early Stage
I assume that we are in an early stage so we don't want to over-engineer things here. I have worked with several start-up companies and this architecture were able to perform very well.

#### 1.2.3 Cost
It is very cost effective for company with small-medium size of data. I'm considering our main cost from,
- Storing/Reading data on S3
- Execution time of functions on Lambda
- Redshift cluster and storage  

The expense associated with Redshift is likely the most significant, and it is crucial to monitor it closely, as the cost escalates with the expanding size of data.

#### 1.2.4 Easy Migration
In the future, chances are we adjust and improve our pipelines. Since Lambda has little learning curve and doesn't require much change in the code, it would be easy to do a migration.

#### 1.2.5 Limitation
I presume we have some limitations from the architecture including,
- **Scalabilty:** Lambda doesn't work with large data or long-running jobs.
- **Cost:** when our data grows bigger to the size of TB, the cost of storing these data on Redshift will be expensive.
- **Lack of distribution processing:** In the future, we might have a requirement for distribution processing and we will likely need to adjust our architecture.

#### 1.3 Alternative
Lastly, I'm well awared of other modern solutions eg,
- Glue
- EMR
- Kubernetes
- Orchestrators

What it all comes down to is they all have their stregths and weaknesses and I believe there's nothing wrong about these architectures. It depends on what we are trying to achieve and we often times need a lot of research and discussion or perhaps a trial and error, in order to come up with the best approach for us :slightly_smiling_face:

## 2. Database Design
I have designed database schema based on a star schema approach from sample json data, separated into facts and dimensions as you can see in the picture below.
![alt text](reference/schema.png)

The design above is based on Shopee data. In the real scenario, we would have another copies of these tables for other marketplaces as well since schema from various marketplaces likely to differ and will get a really high complexity if we maintain all data in the same tables.

### 2.1 Fact Tables
In the picture, we have 2 fact tables including product_sale and order_review.
#### 2.1.1 Product Sale Fact
This table is derived from product.json. It can be used to find out seller performance. The schema contains,
- **id:** a surrogate key.
- **item_id:** a natural key to an item listed on Shopee.
- **shop_id:** a natural key to a seller shop id.
- **discount, sold_count, liked_count:** quantitive data, used for flitering, grouping, etc.

#### 2.1.2 Order Review Fact
This table is derived from review.json, the schema contains,
- **id:** a surrogate key.
- **order_id:** an order id, belonged to a buyer.
- **item_id:** an item associated to the order.
- **cmt_id:** a comment id.
- **user_id:** buyer id.
- **rating:** a star buyer gives to the order.
- **product_quality, seller_service, etc.:** specific rating for each metric.

### 2.2 Dimension Tables
I would like to mention 4 main dimension tables including product, shop, order and comment. The others such as marketplace, user and payment should be having as well in a full architecture.

#### 2.2.1 Product
This table is derived from product.json. It can be used to categorize products and pricing trends of products. The basic schema contains,
- **item_id:** an item id.
- **category_id:** an id of a category.
- **title:** product's title eg, Vitamin.
- **price:** main price of a product.
- **img:** image of the product.
- **sku_props:** product options eg, Vitamin-B, Vitamin-C.
- etc.

#### 2.2.2 Shop
This table is derived from product.json. it stores details of shops. The schema has,
- **shop_id:** a shop_id.
- **shop_location:** a shop location.
- **is_official_shop:** a flag indicating if a shop is an official shop.
- etc.

#### 2.2.3 Order
This table is derived from review.json, the schema contains,
- **order_id:** an order id
- **status:** order status
- **is_repeated_purchased:** a flag indicating if a product was bought by the same user before.
- etc.

#### 2.2.4 Comment
This table is derived from review.json. It stores a review information such as comment. the schema contains,
- **cmt_id:** a comment id.
- **comment:** a review comment
- **delete_reason:** a reason that a comment has been deleted.
- etc.

## 3. Data Quality
### 3.1 Basic Checks
There are some basic validation rules that can be applied for example,
- **Schema check:** verify if data has the same schema as expected
- **Format check:** verify data format of columns eg, email, datetime
- **Nullity check**
- **Duplication check**

### Dealing with anomalies
After performing checks, we can apply data cleansing strategies such as,
- Filling incomplete data
- Remove duplicated records
- Standardizing Data Formats eg, datetime, phone-number
- Raise an error preventing loading data to the sink if needed

### 3.2 Ensure Data Quality
We can apply data quality as parts of our pipeline depending on requirements and expectations. The common parts are:  

#### 3.2.1. Before Transformation/Processing  
Pros:
- This helps us address early issues from sources
- Earliest stage

Cons:
- Source data might be unstructured or complicated before applying transformation

#### 3.2.2. After Transformation/Processing  
Pros:
- Data is more clean and structured
- It can integrate with transformation logic
- Data is already loaded into our script and ready to be examined

Cons:
- Increased Overhead during transformation

#### 3.2.3. Before Ingestion  
Pros:
- Preventing issues at the loading stage

Cons:
- Delayed error detection: this means we store invalid data on S3
- We have to reload data into memory since we usually don't load data during the ingestion process
- If we find an error here, we won't know specifically where the error occurs (source/transformation)

### 3.3 Consideration
#### 3.3.1. Data Quality Goals  
Consider overall data quality goals of our product. If early error detection is crucial, validations before transformation may be more suitable.
#### 3.3.2. Performance Impact  
Evaluate the performance impact of validation checks at each stage, as extensive validation processes can affect the speed and efficiency of data processing.
#### 3.3.3. Business Requirements  
Align data validation processes with business requirements. Some applications may have specific standards regarding data quality.


## 4. Monitoring and Logging
### Monitoring
We can use CloudWatch Alarms and SNS to monitor specific Lambda metrics e.g, errors and duration, trigger notifications when thresholds are breached. This way, we can also observe our Lambda Functions if they have long runtime or consume too much memory.

### Notification
1. Notification Function: A minimal option is to write a notification method in our script, whenever an error occurrs, the error is directly sent to somewhere immediatly eg, Slack, Email.
2. Lambda with SNS Topic: Another option would be integrating Lambda to send error to SNS topic and configure SNS to notify us.

### Logging (Code Level)
We ensure that every pipeline set up a standard logging based on Python Logging module. Basic configurations are,
- Log formatter
- Log level eg, info, warning, error
- Configure custom logger inheriting from Python Root Logger

## 5. Scalability and Maintenance
Assumedly, I'm planning to use Lambda, Python with Polars and Redshift.
### Scalability
- Lambda automatically invoke instances based on incoming frequency
- Lambda function can scale up to 15mins of runtime with 10gb of memory
- Polars can handle data larger than memory with lazy evaluation and streaming

### Maintenance
- Regularly check for updates in marketplace APIs eg, creating a ticket for monthly check
- We use python package management to ensure compatibility of dependencies so our code won't easiliy break
- We build a unittest to help us investigate where an issue arises
- We can also make our test cases automated using CI/CD
- We update schema of tables if needed

## 6. Bonus: Direct Integration with Scrapers
There are 3 options that I can quickly think of,
- Using event-driven tool eg, Lambda
- Using sensor to detect when a file is placed on S3
- Using message queue for real-time processing eg, Kafka, Kinesis

This depends on a use case, how soon we need data to be available.  
That being said, If I'm to implement a streaming pipeline on our use case I would have to discuss more about this.
Significant considerations would be:  
- **Cost:** Streaming pipeline has high cost
- **Batch size:** If we specify batch size = 1 then it might cost us a future. Plus, we will have tons of small files on S3 and it could cause a problem later on
- **Setting up Kafka:** provides more flexibility on configurations and it's great for a long term solution

## 8. Technology Stack
This is a traditional data warehouse approach. Worth mentioning tools and factors to consider would be,

### Redshift
Pros:
- Stable and robust
- Redshift was built to handle large amounts of data and support fast analytical queries
- Redshift is also scalable

Cons:
- We have to add another layer to load data from S3 to Redshift
- Cost gets higher as our datasets growing

Alternatives are:
- RDS
- Athena
- Data lakehouse architecture

### Lambda
Pros:
- Easy to setup
- Cost effective (depends on resource usage, execution time, etc.)
- Suitable for small-medium workloads
- Support event driven triggers

Cons:
- Time and memory limitations

Alternatives are:
- Glue
- Running jobs with orchestrators eg, Airflow
- EMR

### Python
- Python is very robust and has large community support
- Most Data Engineers are familar with Python
- Rich ecosystem of libraries and frameworks for working with ETL processing

### Polars as Data Processing Library
Pros:
- Support lazy evaluation
- Support multi-thread
- Performance on a single machine is better than Spark for many use cases
- No JVM spin-up needed, No cluster needed
- Will be more popular in the future due to its potential

Cons:
- Less maturity compared to Pandas or Spark
- Doesn't yet support distributed processing

Alternatives are:
- Spark
- DuckDB
- Ray
- Pandas
